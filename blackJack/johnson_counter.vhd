----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:45:15 01/12/2016 
-- Design Name: 
-- Module Name:    johnson_counter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity johnson_counter is
    Port ( R : in  STD_LOGIC;
           clk : in  STD_LOGIC;
           data : out  STD_LOGIC_VECTOR (3 downto 0));
end johnson_counter;

architecture Behavioral of johnson_counter is
	signal temp : std_logic_vector(3 downto 0) :=(others => '1');
begin
	data <=temp;
	process(clk,R)
	begin
		if rising_edge(clk) then
			if R = '1' then
				temp <= (others=>'1');
			else 
				temp(3 downto 1) <=temp(2 downto 0);
				temp(0) <= not(temp(3));
			end if;
		end if;
	end process;
end Behavioral;



