
-- VHDL Instantiation Created from source file blackJack_game.vhd -- 19:03:25 01/12/2016
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT blackJack_game
	PORT(
		play : IN std_logic;
		R : IN std_logic;
		plantarse : IN std_logic;
		clk : IN std_logic;          
		display1 : OUT std_logic_vector(6 downto 0);
		display2 : OUT std_logic_vector(6 downto 0);
		display3 : OUT std_logic_vector(6 downto 0);
		display4 : OUT std_logic_vector(6 downto 0)
		);
	END COMPONENT;

	Inst_blackJack_game: blackJack_game PORT MAP(
		play => ,
		R => ,
		plantarse => ,
		clk => ,
		display1 => ,
		display2 => ,
		display3 => ,
		display4 => 
	);


