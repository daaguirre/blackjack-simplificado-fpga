----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:53:25 01/02/2016 
-- Design Name: 
-- Module Name:    FSM - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FSM is
    Port ( inputs : in STD_LOGIC_VECTOR (3 downto 0); --play,plantar,over1, over2
           R : in  STD_LOGIC;
           --clk : in  STD_LOGIC;
           estado : out  STD_LOGIC_VECTOR (2 downto 0);
			  pedir : out std_logic --cambia cuando pides carta
			  );
end FSM;

architecture Behavioral of FSM is
	type estados is (x1,x2,x3,x4,x5,x6);
	signal est_actual:estados:=x1;
	signal pedir_temp : std_logic := '0';
begin
	pedir <= pedir_temp;
	process(R,inputs)
	begin
	--if rising_edge(clk) then
		if R = '1' then
			est_actual<= x1;
		else 
			case est_actual is 
				when x1 => if inputs = "1000" then
									est_actual <=	x2;
							  else 
							  	   est_actual <= x1;
							  end if;
				when x2 => if inputs = "1000" then
									pedir_temp <= '0';
									est_actual <= x3;
							  else
									est_actual <= x2;
									
							  end if;	
				when x3 => if inputs = "1000" then
									est_actual <= x3;
									pedir_temp <= not pedir_temp;
							  elsif inputs = "0100" or inputs = "0010" then
									est_actual <= x4;
							  end if;
				when x4 => if inputs = "1000" then
									pedir_temp <= '0';
									est_actual <= x5;
							  else
									est_actual <= x4;
							  end if;
				when x5 => if inputs = "1000" then
									est_actual <= x5;
									pedir_temp <= not pedir_temp;
							  elsif inputs = "0100" or inputs = "0001" then
									est_actual <= x6;
							  end if;
				when x6 => if inputs = "1000" then
									est_actual <= x6;
							  else 
									est_actual <= x6;
							  end if;
			end case;
		end if;
	--end if;
	end process;
	
	process (est_actual)
	begin
		case est_actual is 
				when x1 => estado <= "001";
				when x2 => estado <= "010";
				when x3 => estado <= "011";
				when x4 => estado <= "100";
				when x5 => estado <= "101";
				when x6 => estado <= "110";
	  end case;
	 end process;
end Behavioral;

