----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:59:28 01/12/2016 
-- Design Name: 
-- Module Name:    top - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top is
    Port ( play : in  STD_LOGIC;
           R : in  STD_LOGIC;
			  plantarse : in  STD_LOGIC;
           clk : in  STD_LOGIC;
           display : out  STD_LOGIC_VECTOR (6 downto 0);
           digctrl : out  STD_LOGIC_VECTOR (3 downto 0));
end top;

architecture Behavioral of top is
	signal ctrl_temp : std_logic_vector(3 downto 0);
	signal display_temp : std_logic_vector(6 downto 0);
	signal temp1 : std_logic_vector(6 downto 0);
	signal temp2 : std_logic_vector(6 downto 0);
	signal temp3 : std_logic_vector(6 downto 0);
	signal temp4 : std_logic_vector(6 downto 0);
	COMPONENT blackJack_game
	PORT(
		play : IN std_logic;
		R : IN std_logic;
		plantarse : IN std_logic;
		clk : IN std_logic;          
		display1 : OUT std_logic_vector(6 downto 0);
		display2 : OUT std_logic_vector(6 downto 0);
		display3 : OUT std_logic_vector(6 downto 0);
		display4 : OUT std_logic_vector(6 downto 0)
		);
	END COMPONENT;
	
	COMPONENT johnson_counter
	PORT(
		R : IN std_logic;
		clk : IN std_logic;          
		data : OUT std_logic_vector(3 downto 0)
		);
	END COMPONENT;
begin
	Inst_blackJack_game: blackJack_game PORT MAP(
		play => play,
		R => R,
		plantarse => plantarse,
		clk => clk,
		display1 => temp1,
		display2 => temp2,
		display3 => temp3,
		display4 => temp4 
	);
	
	Inst_johnson_counter: johnson_counter PORT MAP(
		R => R,
		clk => clk,
		data => ctrl_temp
	);
	
	process(clk)
	begin
	if rising_edge(clk) then
		if ctrl_temp="0111" then
			display_temp <= temp1;
		elsif ctrl_temp="1011" then
			display_temp <= temp2;
		elsif ctrl_temp="1101" then
			display_temp <= temp3;
		elsif ctrl_temp="1110" then
			display_temp <= temp4;
		else 
			display_temp <= "1111111";
		end if;
	end if;
		
	end process;
	
	display <= display_temp; 
	digctrl <= ctrl_temp;

end Behavioral;

