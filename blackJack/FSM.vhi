
-- VHDL Instantiation Created from source file FSM.vhd -- 21:26:06 01/02/2016
--
-- Notes: 
-- 1) This instantiation template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the instantiated module
-- 2) To use this template to instantiate this entity, cut-and-paste and then edit

	COMPONENT FSM
	PORT(
		inputs : IN std_logic_vector(2 downto 0);
		R : IN std_logic;
		clk : IN std_logic;          
		estado : OUT std_logic_vector(2 downto 0)
		);
	END COMPONENT;

	Inst_FSM: FSM PORT MAP(
		inputs => ,
		R => ,
		clk => ,
		estado => 
	);


