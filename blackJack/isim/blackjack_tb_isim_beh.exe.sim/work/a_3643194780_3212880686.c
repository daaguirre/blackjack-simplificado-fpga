/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/home/daaguirre/blackJack/FSM.vhd";
extern char *IEEE_P_2592010699;

unsigned char ieee_p_2592010699_sub_374109322130769762_503743352(char *, unsigned char );


static void work_a_3643194780_3212880686_p_0(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;

LAB0:    xsi_set_current_line(46, ng0);

LAB3:    t1 = (t0 + 1832U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (t0 + 3912);
    t4 = (t1 + 56U);
    t5 = *((char **)t4);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = t3;
    xsi_driver_first_trans_fast_port(t1);

LAB2:    t8 = (t0 + 3800);
    *((int *)t8) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_3643194780_3212880686_p_1(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    unsigned char t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    unsigned int t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    unsigned char t16;
    unsigned int t17;
    char *t18;
    char *t19;
    char *t20;
    static char *nl0[] = {&&LAB6, &&LAB7, &&LAB8, &&LAB9, &&LAB10, &&LAB11};

LAB0:    xsi_set_current_line(50, ng0);
    t1 = (t0 + 1192U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB2;

LAB4:    xsi_set_current_line(53, ng0);
    t1 = (t0 + 1672U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (char *)((nl0) + t3);
    goto **((char **)t1);

LAB2:    xsi_set_current_line(51, ng0);
    t1 = (t0 + 3976);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)0;
    xsi_driver_first_trans_fast(t1);

LAB3:    t1 = (t0 + 3816);
    *((int *)t1) = 1;

LAB1:    return;
LAB5:    goto LAB3;

LAB6:    xsi_set_current_line(54, ng0);
    t5 = (t0 + 1032U);
    t6 = *((char **)t5);
    t5 = (t0 + 6039);
    t4 = 1;
    if (4U == 4U)
        goto LAB15;

LAB16:    t4 = 0;

LAB17:    if (t4 != 0)
        goto LAB12;

LAB14:    xsi_set_current_line(57, ng0);
    t1 = (t0 + 3976);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)0;
    xsi_driver_first_trans_fast(t1);

LAB13:    goto LAB5;

LAB7:    xsi_set_current_line(59, ng0);
    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t1 = (t0 + 6043);
    t3 = 1;
    if (4U == 4U)
        goto LAB24;

LAB25:    t3 = 0;

LAB26:    if (t3 != 0)
        goto LAB21;

LAB23:    xsi_set_current_line(63, ng0);
    t1 = (t0 + 3976);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)1;
    xsi_driver_first_trans_fast(t1);

LAB22:    goto LAB5;

LAB8:    xsi_set_current_line(66, ng0);
    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t1 = (t0 + 6047);
    t3 = 1;
    if (4U == 4U)
        goto LAB33;

LAB34:    t3 = 0;

LAB35:    if (t3 != 0)
        goto LAB30;

LAB32:    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t1 = (t0 + 6051);
    t4 = 1;
    if (4U == 4U)
        goto LAB44;

LAB45:    t4 = 0;

LAB46:    if (t4 == 1)
        goto LAB41;

LAB42:    t8 = (t0 + 1032U);
    t10 = *((char **)t8);
    t8 = (t0 + 6055);
    t16 = 1;
    if (4U == 4U)
        goto LAB50;

LAB51:    t16 = 0;

LAB52:    t3 = t16;

LAB43:    if (t3 != 0)
        goto LAB39;

LAB40:
LAB31:    goto LAB5;

LAB9:    xsi_set_current_line(72, ng0);
    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t1 = (t0 + 6059);
    t3 = 1;
    if (4U == 4U)
        goto LAB59;

LAB60:    t3 = 0;

LAB61:    if (t3 != 0)
        goto LAB56;

LAB58:    xsi_set_current_line(76, ng0);
    t1 = (t0 + 3976);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)3;
    xsi_driver_first_trans_fast(t1);

LAB57:    goto LAB5;

LAB10:    xsi_set_current_line(78, ng0);
    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t1 = (t0 + 6063);
    t3 = 1;
    if (4U == 4U)
        goto LAB68;

LAB69:    t3 = 0;

LAB70:    if (t3 != 0)
        goto LAB65;

LAB67:    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t1 = (t0 + 6067);
    t4 = 1;
    if (4U == 4U)
        goto LAB79;

LAB80:    t4 = 0;

LAB81:    if (t4 == 1)
        goto LAB76;

LAB77:    t8 = (t0 + 1032U);
    t10 = *((char **)t8);
    t8 = (t0 + 6071);
    t16 = 1;
    if (4U == 4U)
        goto LAB85;

LAB86:    t16 = 0;

LAB87:    t3 = t16;

LAB78:    if (t3 != 0)
        goto LAB74;

LAB75:
LAB66:    goto LAB5;

LAB11:    xsi_set_current_line(84, ng0);
    t1 = (t0 + 1032U);
    t2 = *((char **)t1);
    t1 = (t0 + 6075);
    t3 = 1;
    if (4U == 4U)
        goto LAB94;

LAB95:    t3 = 0;

LAB96:    if (t3 != 0)
        goto LAB91;

LAB93:    xsi_set_current_line(87, ng0);
    t1 = (t0 + 3976);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)5;
    xsi_driver_first_trans_fast(t1);

LAB92:    goto LAB5;

LAB12:    xsi_set_current_line(55, ng0);
    t11 = (t0 + 3976);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    t14 = (t13 + 56U);
    t15 = *((char **)t14);
    *((unsigned char *)t15) = (unsigned char)1;
    xsi_driver_first_trans_fast(t11);
    goto LAB13;

LAB15:    t9 = 0;

LAB18:    if (t9 < 4U)
        goto LAB19;
    else
        goto LAB17;

LAB19:    t8 = (t6 + t9);
    t10 = (t5 + t9);
    if (*((unsigned char *)t8) != *((unsigned char *)t10))
        goto LAB16;

LAB20:    t9 = (t9 + 1);
    goto LAB18;

LAB21:    xsi_set_current_line(60, ng0);
    t8 = (t0 + 4040);
    t10 = (t8 + 56U);
    t11 = *((char **)t10);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    *((unsigned char *)t13) = (unsigned char)2;
    xsi_driver_first_trans_fast(t8);
    xsi_set_current_line(61, ng0);
    t1 = (t0 + 3976);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);
    goto LAB22;

LAB24:    t9 = 0;

LAB27:    if (t9 < 4U)
        goto LAB28;
    else
        goto LAB26;

LAB28:    t6 = (t2 + t9);
    t7 = (t1 + t9);
    if (*((unsigned char *)t6) != *((unsigned char *)t7))
        goto LAB25;

LAB29:    t9 = (t9 + 1);
    goto LAB27;

LAB30:    xsi_set_current_line(67, ng0);
    t8 = (t0 + 3976);
    t10 = (t8 + 56U);
    t11 = *((char **)t10);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    *((unsigned char *)t13) = (unsigned char)2;
    xsi_driver_first_trans_fast(t8);
    xsi_set_current_line(68, ng0);
    t1 = (t0 + 1832U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = ieee_p_2592010699_sub_374109322130769762_503743352(IEEE_P_2592010699, t3);
    t1 = (t0 + 4040);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = t4;
    xsi_driver_first_trans_fast(t1);
    goto LAB31;

LAB33:    t9 = 0;

LAB36:    if (t9 < 4U)
        goto LAB37;
    else
        goto LAB35;

LAB37:    t6 = (t2 + t9);
    t7 = (t1 + t9);
    if (*((unsigned char *)t6) != *((unsigned char *)t7))
        goto LAB34;

LAB38:    t9 = (t9 + 1);
    goto LAB36;

LAB39:    xsi_set_current_line(70, ng0);
    t14 = (t0 + 3976);
    t15 = (t14 + 56U);
    t18 = *((char **)t15);
    t19 = (t18 + 56U);
    t20 = *((char **)t19);
    *((unsigned char *)t20) = (unsigned char)3;
    xsi_driver_first_trans_fast(t14);
    goto LAB31;

LAB41:    t3 = (unsigned char)1;
    goto LAB43;

LAB44:    t9 = 0;

LAB47:    if (t9 < 4U)
        goto LAB48;
    else
        goto LAB46;

LAB48:    t6 = (t2 + t9);
    t7 = (t1 + t9);
    if (*((unsigned char *)t6) != *((unsigned char *)t7))
        goto LAB45;

LAB49:    t9 = (t9 + 1);
    goto LAB47;

LAB50:    t17 = 0;

LAB53:    if (t17 < 4U)
        goto LAB54;
    else
        goto LAB52;

LAB54:    t12 = (t10 + t17);
    t13 = (t8 + t17);
    if (*((unsigned char *)t12) != *((unsigned char *)t13))
        goto LAB51;

LAB55:    t17 = (t17 + 1);
    goto LAB53;

LAB56:    xsi_set_current_line(73, ng0);
    t8 = (t0 + 4040);
    t10 = (t8 + 56U);
    t11 = *((char **)t10);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    *((unsigned char *)t13) = (unsigned char)2;
    xsi_driver_first_trans_fast(t8);
    xsi_set_current_line(74, ng0);
    t1 = (t0 + 3976);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)4;
    xsi_driver_first_trans_fast(t1);
    goto LAB57;

LAB59:    t9 = 0;

LAB62:    if (t9 < 4U)
        goto LAB63;
    else
        goto LAB61;

LAB63:    t6 = (t2 + t9);
    t7 = (t1 + t9);
    if (*((unsigned char *)t6) != *((unsigned char *)t7))
        goto LAB60;

LAB64:    t9 = (t9 + 1);
    goto LAB62;

LAB65:    xsi_set_current_line(79, ng0);
    t8 = (t0 + 3976);
    t10 = (t8 + 56U);
    t11 = *((char **)t10);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    *((unsigned char *)t13) = (unsigned char)4;
    xsi_driver_first_trans_fast(t8);
    xsi_set_current_line(80, ng0);
    t1 = (t0 + 1832U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = ieee_p_2592010699_sub_374109322130769762_503743352(IEEE_P_2592010699, t3);
    t1 = (t0 + 4040);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = t4;
    xsi_driver_first_trans_fast(t1);
    goto LAB66;

LAB68:    t9 = 0;

LAB71:    if (t9 < 4U)
        goto LAB72;
    else
        goto LAB70;

LAB72:    t6 = (t2 + t9);
    t7 = (t1 + t9);
    if (*((unsigned char *)t6) != *((unsigned char *)t7))
        goto LAB69;

LAB73:    t9 = (t9 + 1);
    goto LAB71;

LAB74:    xsi_set_current_line(82, ng0);
    t14 = (t0 + 3976);
    t15 = (t14 + 56U);
    t18 = *((char **)t15);
    t19 = (t18 + 56U);
    t20 = *((char **)t19);
    *((unsigned char *)t20) = (unsigned char)5;
    xsi_driver_first_trans_fast(t14);
    goto LAB66;

LAB76:    t3 = (unsigned char)1;
    goto LAB78;

LAB79:    t9 = 0;

LAB82:    if (t9 < 4U)
        goto LAB83;
    else
        goto LAB81;

LAB83:    t6 = (t2 + t9);
    t7 = (t1 + t9);
    if (*((unsigned char *)t6) != *((unsigned char *)t7))
        goto LAB80;

LAB84:    t9 = (t9 + 1);
    goto LAB82;

LAB85:    t17 = 0;

LAB88:    if (t17 < 4U)
        goto LAB89;
    else
        goto LAB87;

LAB89:    t12 = (t10 + t17);
    t13 = (t8 + t17);
    if (*((unsigned char *)t12) != *((unsigned char *)t13))
        goto LAB86;

LAB90:    t17 = (t17 + 1);
    goto LAB88;

LAB91:    xsi_set_current_line(85, ng0);
    t8 = (t0 + 3976);
    t10 = (t8 + 56U);
    t11 = *((char **)t10);
    t12 = (t11 + 56U);
    t13 = *((char **)t12);
    *((unsigned char *)t13) = (unsigned char)5;
    xsi_driver_first_trans_fast(t8);
    goto LAB92;

LAB94:    t9 = 0;

LAB97:    if (t9 < 4U)
        goto LAB98;
    else
        goto LAB96;

LAB98:    t6 = (t2 + t9);
    t7 = (t1 + t9);
    if (*((unsigned char *)t6) != *((unsigned char *)t7))
        goto LAB95;

LAB99:    t9 = (t9 + 1);
    goto LAB97;

}

static void work_a_3643194780_3212880686_p_2(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    static char *nl0[] = {&&LAB3, &&LAB4, &&LAB5, &&LAB6, &&LAB7, &&LAB8};

LAB0:    xsi_set_current_line(96, ng0);
    t1 = (t0 + 1672U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (char *)((nl0) + t3);
    goto **((char **)t1);

LAB2:    t1 = (t0 + 3832);
    *((int *)t1) = 1;

LAB1:    return;
LAB3:    xsi_set_current_line(97, ng0);
    t4 = (t0 + 6079);
    t6 = (t0 + 4104);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    memcpy(t10, t4, 3U);
    xsi_driver_first_trans_fast_port(t6);
    goto LAB2;

LAB4:    xsi_set_current_line(98, ng0);
    t1 = (t0 + 6082);
    t4 = (t0 + 4104);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t1, 3U);
    xsi_driver_first_trans_fast_port(t4);
    goto LAB2;

LAB5:    xsi_set_current_line(99, ng0);
    t1 = (t0 + 6085);
    t4 = (t0 + 4104);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t1, 3U);
    xsi_driver_first_trans_fast_port(t4);
    goto LAB2;

LAB6:    xsi_set_current_line(100, ng0);
    t1 = (t0 + 6088);
    t4 = (t0 + 4104);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t1, 3U);
    xsi_driver_first_trans_fast_port(t4);
    goto LAB2;

LAB7:    xsi_set_current_line(101, ng0);
    t1 = (t0 + 6091);
    t4 = (t0 + 4104);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t1, 3U);
    xsi_driver_first_trans_fast_port(t4);
    goto LAB2;

LAB8:    xsi_set_current_line(102, ng0);
    t1 = (t0 + 6094);
    t4 = (t0 + 4104);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t1, 3U);
    xsi_driver_first_trans_fast_port(t4);
    goto LAB2;

}


extern void work_a_3643194780_3212880686_init()
{
	static char *pe[] = {(void *)work_a_3643194780_3212880686_p_0,(void *)work_a_3643194780_3212880686_p_1,(void *)work_a_3643194780_3212880686_p_2};
	xsi_register_didat("work_a_3643194780_3212880686", "isim/blackjack_tb_isim_beh.exe.sim/work/a_3643194780_3212880686.didat");
	xsi_register_executes(pe);
}
