--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   18:01:33 01/04/2016
-- Design Name:   
-- Module Name:   /home/daaguirre/blackJack/blackjack_tb.vhd
-- Project Name:  blackJack
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: blackJack_game
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY blackjack_tb IS
END blackjack_tb;
 
ARCHITECTURE behavior OF blackjack_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT blackJack_game
    PORT(
         play : IN  std_logic;
         R : IN  std_logic;
         plantarse : IN  std_logic;
         clk : IN  std_logic; 
         display1 : OUT  std_logic_vector(6 downto 0); 
         display2 : OUT  std_logic_vector(6 downto 0);
         display3 : OUT  std_logic_vector(6 downto 0);
         display4 : OUT  std_logic_vector(6 downto 0);
			carta_actual : out STD_LOGIC_VECTOR (7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal play : std_logic := '0';
   signal R : std_logic := '0';
   signal plantarse : std_logic := '0';
   signal clk : std_logic := '0';

 	--Outputs
   signal display1 : std_logic_vector(6 downto 0);
   signal display2 : std_logic_vector(6 downto 0);
   signal display3 : std_logic_vector(6 downto 0);
   signal display4 : std_logic_vector(6 downto 0);
	signal carta_actual: STD_LOGIC_VECTOR (7 downto 0);

   -- Clock period definitions
   constant clk_period : time := 20 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: blackJack_game PORT MAP (
          play => play,
          R => R,
          plantarse => plantarse,
          clk => clk,
          display1 => display1,
          display2 => display2,
          display3 => display3,
          display4 => display4,
			 carta_actual => carta_actual
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 
play_process : process --simulacion de cuando ambos jugadores presionan play 
	begin
	-------------------------------------------------------------------------------
	--INICIO--------------------------------------------
		wait for 10 us;
		play <= '1';--play para empezar partida 
		wait for clk_period*10;
		play <= '0';-- vuelve a 0 porque es un pulsador
		wait for 10 us;
	-------------------------------------------------------------------------------
	--JUGADOR1----------------------------------------------------------------
		play <= '1';-- jugador 1 preparado
		wait for clk_period*10;
		play <= '0';-- vuelve a 0 porque es un pulsador
		wait for 5 us;
		play <= '1'; --pido primera carta
		wait for clk_period*10;
		play <= '0';-- vuelve a 0 porque es un pulsador
		wait for 10 us;
		--para volver a pedir carta hay que presionar play 2 veces
		play <= '1'; --primer pulsacion
		wait for clk_period*10;
		play <= '0';-- vuelve a 0 porque es un pulsador
		wait for clk_period*100;
		play <= '1'; --segunda pulsacion, segunda carta
		wait for clk_period*10;
		play <= '0';-- vuelve a 0 porque es un pulsador
		wait for 10 us;
		plantarse <= '1';--jugador 1 se planta
		wait for clk_period*10;
		plantarse <= '0';--pulsador
		wait for 10 us;
----------------------------------------------------------------------------------------
---JUGADOR2
		play <= '1';-- jugador 2 preparado
		wait for clk_period*10;
		play <= '0';-- vuelve a 0 porque es un pulsador
		wait for 5 us;
		play <= '1'; --pido primera carta
		wait for clk_period*10;
		play <= '0';-- vuelve a 0 porque es un pulsador
		wait for 10 us;
		--para volver a pedir carta hay que presionar play 2 veces
		play <= '1'; --primera pulsacion
		wait for clk_period*10;
		play <= '0';-- vuelve a 0 porque es un pulsador
		wait for clk_period*100;
		play <= '1'; --segunda pulsacion, segunda carta
		wait for clk_period*10;
		play <= '0';-- vuelve a 0 porque es un pulsador
		wait for 10 us;
		plantarse <= '1';--jugador 2 se planta
		wait for clk_period*10;
		plantarse <= '0';--pulsador
		wait for 10 us;
		-------------------------------------------------------
		--Estado final?????????????????????
	end process;

   -- Stimulus process
   stim_proc: process
   begin		      
		wait for 300 us;
      -- insert stimulus here 
		
		assert false
			report "Simulación finalizada"
			severity failure;
   end process;

END;
